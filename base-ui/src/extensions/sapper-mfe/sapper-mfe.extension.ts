import { EnvsMain, EnvsAspect } from '@teambit/envs'
import { ReactAspect, ReactMain } from '@teambit/react'

export class SapperMfeExtension {
  constructor(private react: ReactMain) {}

  static dependencies: any = [EnvsAspect, ReactAspect]

  static async provider([envs, react]: [EnvsMain, ReactMain]) {
    const SapperMfeEnv = react.compose([
      /*
        Use any of the "react.override..." transformers to
      */
    ])

    envs.registerEnv(SapperMfeEnv)

    return new SapperMfeExtension(react)
  }
}
